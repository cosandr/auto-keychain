#!/bin/sh

set -o pipefail

while [ ! -f "/.encryptionroot" ]; do
    echo "ZFS is not ready, waiting"
    sleep 1
done
if [ -n "${cryptdevice}" ]; then
    IFS=: read cryptdev cryptsize cryptoffset <<EOF
$cryptdevice
EOF
else
    echo "Add kernel parameter cryptdevice=<dev>:<size>:<offset>, size and offset in bytes"
    exit
fi

# source zfs hook functions
. /hooks/zfs
# Try for 10 seconds
i=0
while [ $i -lt 10 ]; do
    if dd if="$cryptdev" count=1 bs="$cryptsize" iflag=skip_bytes skip="$cryptoffset" | zfs load-key "$(cat /.encryptionroot)"; then
        echo "Key loaded"
        # kill pending decryption attempt to allow the boot process to continue
        killall zfs
        exit
    fi
    i=$((i+1))
    sleep 1
done

echo "Timeout, cannot decrypt root with key"
exit

