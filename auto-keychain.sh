#!/bin/bash

if [[ $EUID -ne 0 ]]; then
    echo "Run as root"
    exit 1
fi

keystore_dev="/dev/mapper/keystore"
expect_file="$(pwd -P)/auto-keychain.exp"
ex_code=0

if ! fdisk -l "$keystore_dev" &>/dev/null; then
    ./open-keystore.sh
fi

mount=$( grep "$keystore_dev" /proc/mounts )
is_mounted=$?
if [ $is_mounted -ne 0 ]; then
    echo "Mounting keystore"
    mount_point="/mnt/tmp"
    mkdir -p "$mount_point"
    mount "$keystore_dev" "$mount_point"
else
    mount_point=$( awk '{print $2}' <<< "$mount" )
fi

declare -A add_keys=( ["root"]="andrei.key" ["andrei"]="andrei.key cosandr.key" )
for usr in "${!add_keys[@]}"; do
    for key_file in ${add_keys[$usr]}; do
        key_fp="${mount_point}/${key_file}"
        if [[ ! -f "$key_fp" ]]; then
            echo "$key_fp: No such file"
            ex_code=1
        fi
        mapfile -t lines < "$key_fp"
        if  [[ "${#lines[@]}" -ne 2 ]]; then
            echo "$key_fp isn't formatted properly, path on first line, pass on second"
            ex_code=1
            continue
        fi
        if ! sudo -u "$usr" "${expect_file}" "${lines[0]}" "${lines[1]}"; then
            echo "${lines[0]}: Cannot load key"
            ex_code=1
        fi
    done
done

if [[ $ex_code -eq 0 ]]; then
    echo "Unmount keystore"
    umount "$mount_point"
    echo "Close keystore"
    cryptsetup close "$keystore_dev"
fi

unset cryptdev cryptsize cryptoffset

exit $ex_code
