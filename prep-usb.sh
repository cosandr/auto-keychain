#!/bin/dash


# zfs create -V 50m tank/keystore
# cryptsetup luksFormat /dev/zvol/tank/keystore
# cryptsetup open --type luks /dev/zvol/tank/keystore keystore

# Holds key/passphrase for root FS between partitions
ROOT_DISK="/dev/sda"
# Holds SSH key passphrases in LUKS
KEY_DISK="/dev/sdb"

# Leave 1M free for key, format rest with type MS Basic (for FAT32)
sgdisk --zap-all $ROOT_DISK
sgdisk -n1:24K:-1M -t1:0700 $ROOT_DISK

# Write key to disk

echo "myrootpass" > pass.txt
file_size=$( stat --printf="%s" pass.txt )
# seek is sector*512
# part end at 8386527 (4293901824)
write_to=4293901824
dd if=pass.txt of=/dev/sda count=1 bs="$file_size" oflag=seek_bytes seek="$write_to"

# Read to read.txt file
# dd if=/dev/sda of=read.txt count=1 bs="$file_size" iflag=skip_bytes skip="$write_to"

# Read to var
# disk_pass=$( dd if=/dev/sda count=1 bs="$file_size" iflag=skip_bytes skip="$write_to" )
