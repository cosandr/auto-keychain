#!/bin/bash

exec_path="/usr/local/bin/systemd/auto-keychain"

cat > /etc/systemd/system/auto-keychain.service <<EOF
[Unit]
Description=Auto load keychain
After=multi-user.target zfs-mount.service
Requires=multi-user.target zfs-mount.service

[Service]
Type=oneshot
RemainAfterExit=yes
WorkingDirectory=$exec_path
ExecStart=$exec_path/auto-keychain.sh

[Install]
WantedBy=multi-user.target
EOF

cat /etc/systemd/system/auto-keychain.service

mkdir -p "$exec_path"
cp -f -t "${exec_path}" auto-keychain.sh open-keystore.sh auto-keychain.exp
chown root:root -R "${exec_path}"
chmod 755 -R "${exec_path}"

systemctl daemon-reload
