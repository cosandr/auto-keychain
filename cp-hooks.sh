#!/bin/sh

mkdir -p /etc/zfs-read-key
cp readkey_bg.sh /etc/zfs-read-key/readkey_bg.sh
chmod 755 /etc/zfs-read-key/readkey_bg.sh
cp hooks/zfs-read-key /etc/initcpio/hooks/
cp install/zfs-read-key /etc/initcpio/install/
chmod 644 /etc/initcpio/hooks/zfs-read-key
chmod 644 /etc/initcpio/install/zfs-read-key
