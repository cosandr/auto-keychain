#!/bin/dash

set -e

is_root=1
sudo_run="sudo -u andrei "

if test "$(id -u)" -ne "0"; then
    echo "Run as root to unmount devices"
    is_root=0
    sudo_run=""
fi

mount=$( grep "/dev/mapper/keystore" /proc/mounts )
is_mounted=$?
if [ $is_mounted -ne 0 ]; then
    echo "Keystore not mounted"
    exit 1
fi

mount_point=$( echo "$mount" | awk '{print $2}' )

for file in "$mount_point"/*.key
do
    key_path=$( head -1 "$file" )
    key_pass=$( tail -1 "$file" )
    if [ "$key_path" = "$key_pass" ]; then
        echo "$file isn't formatted properly, path on first line, pass on second"
        continue
    fi
    if ! "${sudo_run}./auto-keychain.exp" "$key_path" "$key_pass"; then
        echo "Cannot load key"
        exit 1
    fi
done

[ $is_root -ne 1 ] && exit

device=$( cryptsetup status keystore | grep -o "device:.*" | cut -d' ' -f 3 )
ex=$?
if [ $ex -ne 0 ]; then
    echo "Cannot find crypt device"
    exit 1
fi

umount "$mount_point"
cryptsetup close keystore
eject "$device"
