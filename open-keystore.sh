#!/bin/bash

set -e

mapfile -t lines < /root/keystore.seek
cryptdev="/dev/disk/by-id/${lines[0]}"
cryptsize="${lines[1]}"
cryptoffset="${lines[2]}"
luks_dev="/dev/zd0"

# Wait for cryptdevice to show up for 10 seconds
i=0
cryptdev_found=0
while [ $i -lt 10 ]; do
    if fdisk -l "$cryptdev" &>/dev/null; then
        cryptdev_found=1
        break
    fi
    i=$((i+1))
    echo "Waiting for key device... $i"
    sleep 1
done

if [[ $cryptdev_found -ne 1 ]]; then
    echo "Key device $cryptdev not found"
    exit 1
fi

if ! fdisk -l "$luks_dev" &>/dev/null; then
    echo "Crypt device $luks_dev not found"
    exit 1
fi

disk_pass=$( dd if="$cryptdev" count=1 bs="$cryptsize" iflag=skip_bytes skip="$cryptoffset" )

echo "Opening LUKS device"

cryptsetup open --type luks "$luks_dev" keystore <<< "$disk_pass"

echo "LUKS device opened"

echo "Eject key device"
eject "$cryptdev"

unset disk_pass cryptdev cryptsize cryptoffset
