# Introduction

WARNING: Do not run any of these scripts without looking at them first. Some are VERY DANGEROUS and may wipe your disks.

This is a collection of scripts to read SSH private keys into [Keychain](https://www.funtoo.org/Keychain) from a LUKS encrypted partition whose key is placed "in-between" partitions on a USB disk. There is also a script for loading and unlocking ZFS encrypted rootfs on Arch Linux.

## How To

### USB device prep

Commands for preparing a USB device are in [prep-usb](./prep-usb.sh) (DO NOT RUN as is unless you want to wipe one of your drives). At the end you need to know the following:

1. Device ID (the device name in `/dev/disk/by-id`)
2. Size of your password in bytes
3. Offset of the password

The size of the password can be found by writing it to a file (`echo "myrootpass" > pass.txt`) then using `stat --printf="%s" pass.txt`.

The offset can be found using for example `fdisk -l /dev/sda` and reading the end column of the last partition, this value (number of sectors) must be multiplied by the sector size, which is 512. This number is the `write_to` variable in [prep-usb](./prep-usb.sh).

Once you have these things you must write them to a file (recommended root only) in the order described above on seperate lines (3 lines total), in this repo, that file is at `/root/keystore.seek`

Examples

```sh
    ls -l /dev/disk/by-id
lrwxrwxrwx. 1 root root  9 May 23 10:18 usb-Silicon_Motion_Inc._USB_MEMORY_BAR-0:0 -> ../../sdf
    echo "myrootpass" > pass.txt
    stat --printf="%s" pass.txt
11
    # Assume end sector from fdisk -l /dev/sdf is 8386527, this gives a byte offset of 8386527 * 512 = 4293901824
    # !!! DOUBLE CHECK THE of DEVICE BEFORE RUNNING !!!
    dd if=pass.txt of=/dev/sdf count=1 bs=11 oflag=seek_bytes seek=4293901824
    cat /root/keystore.seek
usb-Silicon_Motion_Inc._USB_MEMORY_BAR-0:0
11
4293901824
    # You may verify that it is correct by running, it should print your password
    dd if=/dev/sdf count=1 bs=11 iflag=skip_bytes skip=4293901824
```

### Unlocking keystore

This is done in [open-keystore](./open-keystore.sh), update `luks_dev` and the seek file location if needed. The key device (USB disk) is ejected after the crypt device is unlocked.

### Adding SSH keys

[auto-keychain](./auto-keychain.sh) handles everything, it is inteded to run automatically at boot. It will try to mount the keystore at `/mnt/tmp` if it is not mounted already (otherwise, the mountpoint is auto-detected). The `add_keys` dictionary defines which users load which SSH key(s), the key is the username and value the key file (see below), multiple files are delimited by spaces.

If all keys were added successfully, the keystore is unmounted and locked. It is kept unlocked for debugging/fixing if there's an error loading a key.

### SSH key files

These are stored as text files on the LUKS partition, the file system doesn't matter. The files must consist of two lines, the first is the absolute path to the SSH private key and the second its password. The filename is what you add to the `add_keys` dictionary.

Example file
```
/home/test/.ssh/id_rsa
passwordforkeyabove
```

### Running on boot

[install-systemd](./install-systemd.sh) copies the requires scripts to `/usr/local/bin/systemd/auto-keychain` and installs a service for auto-keychain. If you are not using a ZFS dataset for storing the LUKS partition, you can remove the zfs-mount constraints.

### ZFS encrypted root

`install` and `hooks` are Arch Linux initramfs hooks for unlocking ZFS encrypted root. They must be loaded after other ZFS hooks and require a kernel parameter similar to the standard crypt hook `cryptdevice=<dev>:<size>:<offset>`, note that `<dev>` needs to be a complete path to the key device (`/dev/disk/by-id/...`), the key device is prepared in the same way as for SSH keys, the same one can be used as well. It will read the password using `dd` and pipe it to `zfs load-key`. I have tested it in a VM and it works well, it does not prevent you from typing the password manually (if the device is unavailable for example).
